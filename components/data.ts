import { NodeDefinition, EdgeDefinition } from 'cytoscape'

// electives:  CS 300 – 400 level courses offered online, excluding the required CS courses for the double degree and CS 410
interface Data {
  [k: string]: {
    title: string
    elective: boolean
    prereqs: string[]
    concurrent_prereqs: string[]
    credits: number
  }
}

const data: Data = {
  CS161: {
    title: 'introduction to computer science i',
    elective: false,
    prereqs: [],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS162: {
    title: 'introduction to computer science ii',
    elective: false,
    prereqs: ['CS161'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS225: {
    title: 'discrete structures in computer science',
    elective: false,
    prereqs: [],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS261: {
    title: 'data structures',
    elective: false,
    prereqs: ['CS162', 'CS225'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS271: {
    title: 'computer architecture and assembly language',
    elective: false,
    prereqs: ['CS161'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS290: {
    title: 'web development',
    elective: false,
    prereqs: ['CS162'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS325: {
    title: 'analysis of algorithms',
    elective: false,
    prereqs: ['CS261', 'CS225'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS340: {
    title: 'introduction to databases',
    elective: false,
    prereqs: ['CS290'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS344: {
    title: 'operating systems i',
    elective: false,
    prereqs: ['CS261', 'CS271'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS361: {
    title: 'software engineering i',
    elective: false,
    prereqs: ['CS261'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS362: {
    title: 'software engineering ii',
    elective: false,
    prereqs: ['CS261'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS467: {
    title: 'online capstone project',
    elective: false,
    prereqs: ['CS344', 'CS361', 'CS362'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS321: {
    title: 'introduction to theory of computation',
    elective: true,
    prereqs: ['CS225', 'CS261'],
    concurrent_prereqs: [],
    credits: 3,
  },
  CS352: {
    title: 'introduction to usability engineering',
    elective: true,
    prereqs: ['CS161'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS370: {
    title: 'introduction to security',
    elective: true,
    prereqs: [],
    concurrent_prereqs: ['CS344'],
    credits: 4,
  },
  CS372: {
    title: 'introduction to computer networks',
    elective: true,
    prereqs: ['CS261', 'CS271'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS373: {
    title: 'defense against the dark arts',
    elective: true,
    prereqs: ['CS344', 'CS340', 'CS372'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS381: {
    title: 'programming language fundamentals',
    elective: true,
    prereqs: ['CS261', 'CS225'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS391: {
    title: 'social and ethical issues in computer science',
    elective: true,
    prereqs: [],
    concurrent_prereqs: [],
    credits: 3,
  },
  CS427: {
    title: 'cryptography',
    elective: true,
    prereqs: ['CS261'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS444: {
    title: 'operating systems ii',
    elective: true,
    prereqs: ['CS344', 'CS271'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS450: {
    title: 'introduction to computer graphics',
    elective: true,
    prereqs: ['CS261'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS464: {
    title: 'open source software',
    elective: true,
    prereqs: ['CS261'],
    concurrent_prereqs: ['CS361'],
    credits: 4,
  },
  CS475: {
    title: 'introduction to parallel programming',
    elective: true,
    prereqs: ['CS261'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS477: {
    title: 'introduction to digital forensics',
    elective: true,
    prereqs: ['CS344', 'CS370'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS478: {
    title: 'network security',
    elective: true,
    prereqs: ['CS372'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS492: {
    title: 'mobile software development',
    elective: true,
    prereqs: ['CS344'],
    concurrent_prereqs: [],
    credits: 4,
  },
  CS493: {
    title: 'cloud application development',
    elective: true,
    prereqs: ['CS290', 'CS340', 'CS372'],
    concurrent_prereqs: [],
    credits: 4,
  },
}

interface Node extends NodeDefinition {
  data: {
    id: string
    label: string
  }
  classes: string[]
}

interface Edge extends EdgeDefinition {
  data: {
    id: string
    source: string
    target: string
  }
  classes: string[]
}

let nodes: Node[] = []
let edges: Edge[] = []
for (const [course, course_data] of Object.entries(data)) {
  let node: Node = {
    data: { id: course, label: course_data.title.toUpperCase() },
    classes: [],
  }

  if (course_data.elective) {
    node.classes = ['elective']
  }

  nodes.push(node)

  course_data.prereqs.forEach((prereq) =>
    edges.push({
      data: { id: `${prereq}->${course}`, source: prereq, target: course },
      classes: [],
    })
  )

  course_data.concurrent_prereqs.forEach((concurrent_prereq) =>
    edges.push({
      data: {
        id: `${concurrent_prereq}->${course}`,
        source: concurrent_prereq,
        target: course,
      },
      classes: ['concurrent'],
    })
  )
}

export { data }
export const elements = [...nodes, ...edges]
export type { Data, Node, Edge }

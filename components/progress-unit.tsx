import ProgressIcon from './progress-icon'
import styles from './progress-unit.module.css'

const ProgressUnit = ({
  current,
  goal,
  units,
}: {
  current: number
  goal: number
  units: string
}) => {
  const width = (current / goal) * 100
  return (
    <span
      className={`flex items-center text-xs rounded-full m-1 h-4/5 bg-gradient-to-r from-blue-700 via-emerald-700 to-green-700 dark:from-blue-500 dark:via-emerald-500 dark:to-green-500 relative overflow-hidden ${
        width >= 100 ? styles.complete : ''
      }`}
    >
      <div className="flex items-center justify-center p-1 z-10 text-white dark:text-black">
        <ProgressIcon complete={current >= goal} />
        {current}/{goal} {units}
      </div>
      {width !== 100 && (
        <span
          className="h-full absolute bg-red-400 dark:bg-red-200 top-0 right-0"
          style={{
            width: `${100 - width}%`,
          }}
        ></span>
      )}
    </span>
  )
}

export default ProgressUnit

import Image from 'next/image'
import gitlabLogo from '../public/gitlab-icon-rgb.svg'

const GitLabLink = ({ href, alt }: { href: string; alt: string }) => {
  return (
    <a
      href={href}
      target="_blank"
      rel="noopener noreferrer"
      className="w-10 h-full"
    >
      <div className="w-full h-full flex justify-center items-center relative">
        <Image src={gitlabLogo} alt={alt} layout="fill" objectFit="contain" />
      </div>
    </a>
  )
}

export default GitLabLink

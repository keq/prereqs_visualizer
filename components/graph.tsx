import { useEffect, useRef, Dispatch, SetStateAction } from 'react'
import cytoscape, { BreadthFirstLayoutOptions, NodeSingular } from 'cytoscape'
import popper from 'cytoscape-popper'
import tippy, { Instance, Props } from 'tippy.js'
import 'tippy.js/dist/tippy.css'
import _ from 'lodash'

import { elements, Node, Edge } from './data'

interface NodeSingularTippable extends NodeSingular {
  tip?: Instance<Props>
}
interface NodeSingularTipped extends NodeSingular {
  tip: Instance<Props>
}

interface Palette {
  [k: string]: {
    [k: string]: string
  }
}
const COLORS: Palette = {
  '': {
    bg: '#fefefe',
    fg: '#282c34',
    green: '#5dbd7a',
    red: '#d82148',
    orange: 'orange',
  },
  dark: {
    bg: '#282c34',
    fg: '#fefefe',
    green: 'lightgreen',
    red: 'hotpink',
    orange: 'orange',
  },
}

const Graph = ({
  theme,
  setCompleted,
}: {
  theme: string
  setCompleted: Dispatch<SetStateAction<string[]>>
}) => {
  const col = (color: string) => COLORS[theme][color]

  const addClass = (ele: Node | Edge, cls: string) => {
    if (!ele.classes.includes(cls)) {
      ele.classes.push(cls)
    }
  }
  const rmClass = (ele: Node | Edge, cls: string) => {
    if (ele.classes.includes(cls)) {
      ele.classes = ele.classes.filter((c) => c != cls)
    }
  }

  const complete = (id: string, save: boolean = true) => {
    const ele = elements.find((n) => n.data.id == id)
    if (ele) {
      addClass(ele, 'diminished')
      const storedDiminished = JSON.parse(
        localStorage.getItem('diminished') || '{}'
      )
      if (save) {
        if (storedDiminished !== null) {
          storedDiminished[id] = true
          localStorage.setItem('diminished', JSON.stringify(storedDiminished))
        } else {
          localStorage.setItem('diminished', JSON.stringify({ id: true }))
        }
      }
      if (!ele.data.id.includes('->')) {
        setCompleted((completed) => [...completed, id])
      }
    }
  }
  const uncomplete = (id: string, save: boolean = true) => {
    const ele = elements.find((n) => n.data.id == id)
    if (ele) {
      rmClass(ele, 'diminished')
      if (save) {
        const storedDiminished = JSON.parse(
          localStorage.getItem('diminished') || '{}'
        )
        if (storedDiminished !== null) {
          delete storedDiminished[id]
          localStorage.setItem('diminished', JSON.stringify(storedDiminished))
        }
      }
      if (!ele.data.id.includes('->')) {
        setCompleted((completed) => completed.filter((c: string) => c !== id))
      }
    }
  }

  const container = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const storedDiminished = JSON.parse(
      localStorage.getItem('diminished') || '{}'
    )
    let courses = []
    for (const id of Object.keys(storedDiminished)) {
      complete(id, false)
      if (!id.includes('->')) {
        courses.push(id)
      }
    }
    setCompleted(courses)
  }, [])

  useEffect(() => {
    if (!container.current) {
      return
    }

    const TRANS_DUR = 200
    const TRANS_TIME_FUNC = 'ease-in-out-sine'

    // media query for portrait orientation
    const portrait = () => {
      const mql = window.matchMedia('(orientation: portrait)')
      return mql.matches
    }

    // media query for narrow aspect ratios
    const narrow = () => {
      const mql = window.matchMedia('(max-aspect-ratio: 3/4)')
      return mql.matches
    }

    const main = () => {
      // outdated type def:
      // https://github.com/DefinitelyTyped/DefinitelyTyped/discussions/58920
      const layout: BreadthFirstLayoutOptions = {
        name: 'breadthfirst',
        directed: true,
        maximal: true,
      }
      // rotate layout 90 degrees if portrait orientation
      // and squeeze a bit if very narrow
      // narrow check needs to come before portrait since it is a subset
      if (narrow()) {
        layout.transform = (_, pos) => {
          return { x: pos.y / 1.5, y: pos.x * 1.5 }
        }
      } else if (portrait()) {
        layout.transform = (_, pos) => {
          return { x: pos.y, y: pos.x }
        }
      }

      const cy = cytoscape({
        container: container.current,
        layout,
        style: [
          {
            selector: 'node',
            style: {
              content: 'data(id)',
              'text-valign': 'center',
              'text-halign': 'center',
              'font-family': '"Ubuntu Mono", monospace',
              'font-weight': 700,
              'font-size': '2em',
              color: col('bg'),
              'background-color': col('fg'),
              shape: 'round-rectangle',
              height: 90,
              width: 90,
              'transition-property': 'background-color height width font-size',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: 'node.elective',
            style: {
              shape: 'ellipse',
            },
          },
          {
            selector: 'edge',
            style: {
              'curve-style': 'straight',
              'target-arrow-shape': 'triangle',
              width: 5,
              'arrow-scale': 1.5,
              'transition-property': 'target-arrow-color line-color',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: 'edge.concurrent',
            style: {
              'line-style': 'dashed',
            },
          },
          {
            selector: '.hl1',
            style: {
              'target-arrow-color': col('green'),
              'line-color': col('green'),
              'background-color': col('green'),
              'transition-property':
                'target-arrow-color line-color background-color',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: '.hl2',
            style: {
              'target-arrow-color': col('red'),
              'line-color': col('red'),
              'background-color': col('red'),
              'transition-property':
                'target-arrow-color line-color background-color',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: '.hl3',
            style: {
              'background-color': col('orange'),
              'transition-property': 'background-color',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: 'node.hl1,node.hl2,node.hl3',
            style: {
              height: 120,
              width: 120,
              'font-size': '2.8em',
              'transition-property': 'height width font-size',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
          {
            selector: '.diminished',
            style: {
              color: col('fg'),
              opacity: 0.15,
              'transition-property': 'color opacity height width font-size',
              'transition-duration': TRANS_DUR,
              'transition-timing-function': TRANS_TIME_FUNC,
            },
          },
        ],
        elements,
      })

      // disable user pan, zoom, select
      cy.userPanningEnabled(false)
      cy.userZoomingEnabled(false)
      cy.boxSelectionEnabled(false)
      cy.autounselectify(true)

      if (!cy.getElementById(elements[0].data.id).popperRef) {
        cytoscape.use(popper)
      }

      // function to attach tooltip to node
      const makePopper = (node: NodeSingularTippable) => {
        let ref = node.popperRef()

        let tip = tippy(document.createElement('div'), {
          getReferenceClientRect: ref.getBoundingClientRect,
          trigger: 'manual',
          content: () => {
            let content = document.createElement('div')
            const help = '<small><i>(click/double-tap to show/hide)</i></small>'
            const inner = `${node.data().label}<br>${help}`
            content.innerHTML = inner
            return content
          },
          theme: 'info',
        })
        node.tip = tip
      }

      cy.ready(() => {
        cy.nodes().forEach((ele) => {
          makePopper(ele)
        })
      })

      const diminish = (node: NodeSingularTipped) => {
        const course: string = node.data().id
        if (!node.hasClass('diminished')) {
          node.addClass('diminished')
          node.connectedEdges().addClass('diminished')
          complete(course)
          node.connectedEdges().forEach((edge) => complete(edge.id()))
          hlOff(node)
        } else {
          // undiminish node and uncomplete the course
          node.removeClass('diminished')
          uncomplete(course)
          // undiminish and uncomplete edges that are connected to undiminished nodes
          node.outgoers().forEach((outgoer) => {
            if (outgoer.isEdge() && !outgoer.target().hasClass('diminished')) {
              outgoer.removeClass('diminished')
              uncomplete(outgoer.id())
            }
          })
          node.incomers().forEach((outgoer) => {
            if (outgoer.isEdge() && !outgoer.source().hasClass('diminished')) {
              outgoer.removeClass('diminished')
              uncomplete(outgoer.id())
            }
          })
        }
      }
      const hlOn = (node: NodeSingularTipped) => {
        if (!node.hasClass('diminished')) {
          node.tip.show()
          node.addClass('hl3')
          const unlocks = [node.outgoers(), node.outgoers().targets()]
          const prereqs = [node.incomers(), node.incomers().sources()]

          unlocks.forEach((unlock) => unlock.addClass('hl1'))
          prereqs.forEach((prereq) => prereq.addClass('hl2'))
        }
      }
      const hlOff = (node: NodeSingularTipped) => {
        node.tip.hide()

        node.removeClass('hl3')

        const unlocks = [node.outgoers(), node.outgoers().targets()]
        const prereqs = [node.incomers(), node.incomers().sources()]
        unlocks.forEach((unlock) => unlock.removeClass('hl1'))
        prereqs.forEach((prereq) => prereq.removeClass('hl2'))
      }

      cy.on('click dbltap', 'node', (evt) => diminish(evt.target))
      cy.on('mouseover', 'node', (evt) => hlOn(evt.target))
      cy.on('mouseout', 'node', (evt) => hlOff(evt.target))

      // exclusively touchscreen inputs
      // tap on node to highlight and reset other highlights
      cy.on('touchstart', 'node', (evt) => {
        const node = evt.target
        cy.$('').removeClass('hl1 hl2 hl3')
        hlOn(node)
      })
      // tap on nonelement to reset all highlights
      cy.on('touchstart', (evt) => {
        if (evt.target === cy) {
          cy.$('').removeClass('hl1 hl2 hl3')
        }
      })
    }
    main()

    // completely regraph every time window is resized with some
    // debouncing to rate limit regraphing
    if (container.current) {
      const ro = new ResizeObserver(_.debounce(main, 200))
      ro.observe(container.current)
    }
  }, [theme])

  return <div className="h-full w-full bg-inherit" ref={container} />
}

export default Graph

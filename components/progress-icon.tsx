import { BsCheck, BsX } from 'react-icons/bs'

const ProgressIcon = ({ complete }: { complete: boolean }) => {
  return <span className="text-lg">{complete ? <BsCheck /> : <BsX />}</span>
}

export default ProgressIcon

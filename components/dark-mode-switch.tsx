import { Dispatch, SetStateAction, useEffect } from 'react'
import { BsFillMoonStarsFill, BsFillSunFill } from 'react-icons/bs'

const DarkModeToggle = ({
  theme,
  setTheme,
}: {
  theme: string
  setTheme: Dispatch<SetStateAction<string>>
}) => {
  useEffect(() => {
    const storedTheme = localStorage.getItem('theme')
    if (storedTheme) {
      setTheme(storedTheme)
    }
  }, [])

  useEffect(() => {
    const root = window.document.documentElement

    if (theme === 'dark') {
      root.classList.add('dark')
    } else {
      root.classList.remove('dark')
    }
    localStorage.setItem('theme', theme)
  }, [theme])

  const dark = (
    <button title="Switch to Dark Mode" onClick={() => setTheme('dark')}>
      <BsFillMoonStarsFill className="text-black" />
    </button>
  )
  const light = (
    <button title="Switch to Light Mode" onClick={() => setTheme('')}>
      <BsFillSunFill className="text-white" />
    </button>
  )
  return <div className="p-2 flex">{theme === 'dark' ? light : dark}</div>
}

export default DarkModeToggle

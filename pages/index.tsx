import { useState } from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'

import Graph from '../components/graph'
import Progress from '../components/progress'
import GitLabLink from '../components/gitlab-link'
import DarkModeSwitch from '../components/dark-mode-switch'
import Script from 'next/script'

const Home: NextPage = () => {
  const [theme, setTheme] = useState('dark')
  const [completed, setCompleted] = useState<string[]>([])

  return (
    <div className="h-screen justify-center items-center">
      <Head>
        <title>Prereqs Visualizer</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Script
        data-domain="reqs.zdsfa.com"
        src="/pls/pls.js"
        data-api="/pls/event"
      />

      <div className="w-full h-full flex flex-col bg-white dark:bg-zinc-900">
        <header className="bg-zinc-100 dark:bg-zinc-800 flex justify-center items-center h-10">
          <DarkModeSwitch theme={theme} setTheme={setTheme} />
          <Progress completed={completed} />
          <GitLabLink
            href="https://gitlab.com/keq/prereqs_visualizer"
            alt="Link to GitLab Repo"
          />
        </header>
        <Graph setCompleted={setCompleted} theme={theme} />
      </div>
    </div>
  )
}

export default Home

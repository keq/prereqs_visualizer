import { DependencyList, EffectCallback, useEffect, useRef } from 'react'

/** useEffect hook that only runs the effect callback after a given number of dependency changes */
const useEffectAfter = (
  startAfter: number,
  func: EffectCallback,
  deps: DependencyList | undefined
) => {
  const runs = useRef(0)

  useEffect(() => {
    if (runs.current < startAfter) {
      runs.current += 1
    } else {
      func()
    }
  }, deps)
}

export default useEffectAfter
